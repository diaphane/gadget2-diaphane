
Gadget2-diaphane: Gadget2 with Diaphane support

Diaphane Library: "DIAPHANE: A portable radiation transport library for astrophysical applications", Computer Physics Communications Volume 226, May 2018, Pages 1-9 https://www.sciencedirect.com/science/article/pii/S0010465517303922

Gadget2: Springel V., 2005, MNRAS, 364, 1105 Springel V., Yoshida N., White S. D. M., 2001, New Astronomy, 6, 51 https://wwwmpa.mpa-garching.mpg.de/gadget/

Gadget2 information can be found here:
https://wwwmpa.mpa-garching.mpg.de/gadget/

-Darren Reed for the Diaphane Team

